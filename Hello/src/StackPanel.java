import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Ellipse2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

class StackPanel extends JPanel {  
  
  public static final int PANEL_WIDTH = 800;
  public static final int PANEL_HEIGHT = 500;
  private static final int BALL_DIAMETER = 100;
  private static final Color transGray = new Color(204,204,204, 240); 
	public BouncyBall[] balls;
	public BouncyBall[] stack;
	public int currentStackElement;
	public double rate;
	public Timer timer;
	public boolean stackInitialized;
	public ArrayList<String> code;


public StackPanel() {
    setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    setBackground(new Color(233,245,248));
    setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
    setBackground(new Color(250,244,227));

    
    balls = new BouncyBall[5];
  
    for (int i = 0; i < balls.length; i++) {
    	Random rand = new Random();
    	int randomX = rand.nextInt(PANEL_WIDTH - BALL_DIAMETER);
    	int randomY = rand.nextInt(PANEL_HEIGHT - BALL_DIAMETER);
    	double dx =  Math.random() * 2 - 1;
    	double dy =  Math.random() * 2 - 1;
    	balls[i] = new BouncyBall(randomX, randomY, BALL_DIAMETER, Color.white, dx, dy);
    }

    Color red = new Color(174,23,2);
    Color blue = new Color(52,54,94);
    Color green = new Color(192,210,62);
    Color yellow = new Color(237,174,28);
    Color purple = new Color(150,59,74);
    
    balls[0].setColor(red); balls[0].setName("red");
    balls[1].setColor(blue); balls[1].setName("blue");
    balls[2].setColor(green); balls[2].setName("green");
    balls[3].setColor(yellow); balls[3].setName("yellow");
    balls[4].setColor(purple); balls[4].setName("purple");
    
    currentStackElement = -1;
    code = new ArrayList<>();
    
    class Animator implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {

			repaint();
		}
    }
    
    class MyMouseListener implements MouseListener, MouseMotionListener {
    	
    	private Point coords;
    	private BouncyBall activeBall;
		@Override
		public void mouseClicked(MouseEvent event) {
			if (!stackInitialized) {
				if (new Rectangle2D.Double(PANEL_WIDTH - 220, 20, 200, 60).contains(event.getPoint())) {
					stack = new BouncyBall[5];
					for (int i = 0; i < stack.length; i++) {
						stack[i] = new BouncyBall(30, -BALL_DIAMETER + PANEL_HEIGHT - i * BALL_DIAMETER, BALL_DIAMETER, transGray, 0, 0);
					}
					currentStackElement = 0;
					stackInitialized = true;
					code.add("Stack<Ball> stack = new Stack<>();");
				}
			}
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent event) {
	
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			coords = null;
			activeBall = null;
				
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if (coords == null || activeBall == null) 
				for (BouncyBall ball : balls) {
					if (ball == null) continue;
					if (ball.contains(e.getPoint())) {
						coords = e.getPoint();
						activeBall = ball;
					}
				}
			
			if (coords == null || activeBall == null) {
				return;
			}
			activeBall.setDx(e.getX() - coords.getX());
			activeBall.setDy(e.getY() - coords.getY());
			coords = e.getPoint();
	
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
    	
    }
    MyMouseListener ml = new MyMouseListener();
    addMouseListener(ml);
    addMouseMotionListener(ml);
    timer = new Timer(20, new Animator());
    timer.start();
  }

  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D)g;  
    for (BouncyBall ball : balls) {
    	if (ball == null) continue;
		bounce(ball);
		ball.move();
		ball.paint(g2);
	}
    
    if (!stackInitialized) {
    	drawPromptButton(g2, "Initialize Stack");
    }
    else {
    	drawPromptButton(g2, "Fill the Stack");
    	drawCodeBox(g2, code);
	    drawStack(g2);
	    highlight(g2);
    }
  }
  
  private void highlight(Graphics2D g2) {
		Color orangeTransparent = new Color(225,94,50, 230);
		if (currentStackElement >= 5) return;
		boolean intersection = false;
		for (int i = 0; i < 5 && !intersection; i++) {
			if (balls[i] != null) {
				BouncyBall current = stack[currentStackElement];
				if (balls[i].intersects(current.getBounds2D())) {
					if (overlap(current, balls[i])) {
						stack[currentStackElement] = new BouncyBall(current.getX(), current.getY(), BALL_DIAMETER, balls[i].getColor(), 0, 0);
						stack[currentStackElement].setName(balls[i].getName());
						balls[i] = null;
						code.add("stack.push(" + stack[currentStackElement].getName() + ");");
						currentStackElement++;
						
						return;
					}
					intersection = true;
				}
			}
		}
		if (intersection) {
			stack[currentStackElement].setColor(orangeTransparent);
		}
		else {
			stack[currentStackElement].setColor(new Color(160, 160, 160, 240));
		}
  }

	private boolean overlap(BouncyBall b1, BouncyBall b2) {
		double x1 = b1.getX();
		double y1 = b1.getY();
		double x2 = b2.getX();
		double y2 = b2.getY();
		
		if ((Math.abs(x1 - x2) <= 14) && (Math.abs(y1 - y2) <= 14)) {
			return true;
		}
		return false;
	}

	private void drawStack(Graphics2D g2) {
		
		for (int i = 0; i < 5; i++) {
	
			stack[i].paint(g2);
		}
	}

	private void drawPromptButton(Graphics2D g2, String text) {
		g2.setColor(transGray);
	    g2.fill(new Rectangle2D.Double(PANEL_WIDTH - 230, 20, 210, 60));
	    g2.setColor(Color.black);
	    g2.setFont(new Font ("Arial", 0, 26));
	    g2.drawString(text, PANEL_WIDTH - 200, 60);
	}
	
	private void drawCodeBox(Graphics2D g2, ArrayList<String> text) {
		g2.setColor(transGray);
	    g2.fill(new Rectangle2D.Double(PANEL_WIDTH - 230, 100, 210, PANEL_HEIGHT - 120));
	    g2.setColor(Color.black);
	    g2.setFont(new Font ("Arial", 0, 12));
	    for (int i = 0; i < text.size(); i++) {
	    	g2.drawString(text.get(i), PANEL_WIDTH - 220, 120 + i * 20);
	    }
	}

  
  private void bounce(BouncyBall ball) {
	  Shape screen = new Rectangle2D.Double(0, 0, PANEL_WIDTH, PANEL_HEIGHT);
	  if (!screen.contains(ball.getBounds2D())){
       
		  if (ball.getBounds2D().intersects(0, -1, PANEL_WIDTH, 1)) {
			  ball.setDy(Math.random() + 0.5 + rate); 
		  }
		  else if (ball.getBounds2D().intersects(0, PANEL_HEIGHT, PANEL_WIDTH, 1)){  
			  ball.setDy(-(Math.random() + 0.5 + rate));
		  }
		  else if (ball.getBounds2D().intersects(-1, 0, 1, PANEL_HEIGHT) ) {
			  ball.setDx(Math.random() + 0.5 + rate);
		  } 
		  else if (ball.getBounds2D().intersects(PANEL_WIDTH, 0, 1, PANEL_HEIGHT)) {
			  ball.setDx(-(Math.random() + 0.5 + rate));
		  }
		  else {
			 
			  if (ball.getBounds2D().getX() > PANEL_WIDTH) {
				  ball.setDx(- Math.abs(ball.getDx()));
			  }
			  if (ball.getBounds2D().getX() < 0) {
				  ball.setDx(Math.abs(ball.getDx()));
			  }
			  if (ball.getBounds2D().getY() > PANEL_HEIGHT) {
				  ball.setDy(- Math.abs(ball.getDy()));
			  }
			  if (ball.getBounds2D().getY() < 0) {
				  ball.setDy(Math.abs(ball.getDy()));
			  }
		  }
	  }
  }
  
  private class StackGraph {
	  
	  public StackGraph(int num) {
		
	  }
  }
  
	private class BouncyBall implements Shape {
		
		private String name;
		private Ellipse2D circle;
		private Color color;
		private double x;
		private double y;
		private double dx;
		private double dy;
		private double diameter;
		
		public BouncyBall(double x0, double y0, double diameter, Color color, double dx, double dy) {
			this.setColor(color);
			this.setDx(dx);
			this.setDy(dy);
			this.diameter = diameter;
			x = x0;
			y = y0;
			circle = new Ellipse2D.Double(x0, y0, diameter, diameter);
		}
		
		public String getName() {
		
			return name;
		}

		public void setName(String string) {
			name = string;
			
		}

		public void setY(int i) {
			y = i;
			
		}

		public void setX(int i) {
			x = i;
			
		}

		public void paint(Graphics2D g2) {
			g2.setColor(getColor());
		    g2.fill(circle);
		}

		public double getX() {
			return x;
		}

		private double getY() {
			return y;
		}
		
		private void move() {
			x += dx;
			y += dy;
			circle = new Ellipse2D.Double(x, y, diameter, diameter);
		}
		
		
		@Override
		public boolean contains(Point2D arg0) {
			return circle.contains(arg0);
		}

		@Override
		public boolean contains(Rectangle2D arg0) {
			return circle.contains(arg0);
		}

		@Override
		public boolean contains(double arg0, double arg1) {
			return circle.contains(arg0,arg1);
		}

		@Override
		public boolean contains(double arg0, double arg1, double arg2, double arg3) {
			return circle.contains(arg0,arg1,arg2,arg3);
		}

		@Override
		public Rectangle getBounds() {
			return circle.getBounds();
		}

		@Override
		public Rectangle2D getBounds2D() {
			return circle.getBounds2D();
		}

		@Override
		public PathIterator getPathIterator(AffineTransform arg0) {
			return circle.getPathIterator(arg0);
		}

		@Override
		public PathIterator getPathIterator(AffineTransform arg0, double arg1) {
			return circle.getPathIterator(arg0, arg1);
		}

		@Override
		public boolean intersects(Rectangle2D arg0) {
			return circle.intersects(arg0);
		}

		@Override
		public boolean intersects(double arg0, double arg1, double arg2, double arg3) {
			return circle.intersects(arg0, arg1, arg2, arg3);
		}

		public Color getColor() {
			return color;
		}

		public void setColor(Color color) {
			this.color = color;
		}

		public double getDx() {
			return dx;
		}

		public void setDx(double dx) {
			this.dx = dx;
		}

		public double getDy() {
			return dy;
		}

		public void setDy(double dy) {
			this.dy = dy;
		}

	}
}