import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;


public class AccountManager {
	
	String user;
	String generatedPassword;
	
	public AccountManager(String user, String password, boolean existing) throws FileNotFoundException {
		// to do
	}
	
	
	
	/*
	 * Borrowed from: http://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/
	 */
	public static boolean authenticate(String user, String password) 
    {
        String passwordToHash = password;
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(passwordToHash.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
        Scanner in = null;
        try {
			in = new Scanner(new File("accounts.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        while (in.hasNextLine()) {
        	String line = in.nextLine();
        	String[] words = line.split(" ");
        	if (words[0].equals(user)) {
        		return words[1].equals(generatedPassword);
        	}
        }
        
        in.close();
        return false;
    }
}


