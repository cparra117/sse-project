import javax.swing.*;


public class GraphicsPanelTester extends JApplet {
  public static void main(String s[]) {
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Test");
    JApplet applet = new GraphicsPanelTester();
    applet.init();
    frame.getContentPane().add(applet);
    frame.pack();
    frame.setVisible(true);
    //frame.setResizable(false);
  }
  
  public void init() {
    JPanel panel = new StackPanel();
    getContentPane().add(panel);
  }
}


  
