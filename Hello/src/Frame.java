import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.awt.Button;
import java.awt.GridLayout;

public class Frame extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JPanel graphPanel;
	private int theme;
	private boolean userAuth;
	private String username;

	/**
	 * Create the frame.
	 */
	public Frame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 570);
		setResizable(false);
		Color[] colors = {new Color(245,255,250), new Color(227,91,93), new Color(115,168,175)};
		theme = 0;
		
		JMenuBar menuBar = new JMenuBar();
		

		JMenuItem menu0 = new JMenuItem("Log in");
		menu0.setMnemonic(KeyEvent.VK_L);
		menu0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				PopUp pop = new PopUp();
				//System.out.println(pop.authenticated());
				if (pop.authenticated()) {
					userAuth = true;
					username = pop.getUser();
					//System.out.println(userAuth + " " + username);
				}
			}
		});
		
		Dimension btnSize = new Dimension(80, 30);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(Color.lightGray);
		setContentPane(contentPane);

		panel = new JPanel();
		contentPane.add(panel, BorderLayout.WEST);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		//panel_1.setPreferredSize(new Dimension (160, 550));
		panel.add(panel_1, BorderLayout.WEST);
		
		JPanel panel_4 = new JPanel();
		
		JMenuItem menu1 = new JMenuItem("Settings");
		menu1.setMnemonic(KeyEvent.VK_S);
		menu1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg2) {
				SettingsPopUp pop = new SettingsPopUp(contentPane, panel, panel_4, colors);
			}
		});

		JMenuItem menu2 = new JMenuItem("About");
		menu2.setMnemonic(KeyEvent.VK_A);
		menu2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg2) {
				AboutPopUp about = new AboutPopUp();
			}
		});
	
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mntmHelp.setMnemonic(KeyEvent.VK_H);
		mntmHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg2) {
				HelpPopUp help = new HelpPopUp();
			}
		});
		
		menuBar.add(menu0);
		menuBar.add(menu1);
		menuBar.add(menu2);
		menuBar.add(mntmHelp);
		setJMenuBar(menuBar);
		panel_1.setLayout(new GridLayout(0, 1, 0, 0));
		
		
		panel_1.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 1, 0, 0));
		
		Component verticalGlue = Box.createVerticalGlue();
		panel_4.add(verticalGlue);
		
		Component verticalGlue_1 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_1);
		
		Component verticalGlue_2 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_2);
		
		Component verticalGlue_3 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_3);
		
		Component verticalGlue_4 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_4);
		
		Component verticalGlue_5 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_5);
		
		Component verticalGlue_6 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_6);
		
		Component verticalGlue_7 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_7);
		
		Component verticalGlue_8 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_8);
		
		Component verticalGlue_9 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_9);
		
		Component verticalGlue_10 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_10);
		
		Component verticalGlue_11 = Box.createVerticalGlue();
		panel_4.add(verticalGlue_11);
		
		
		
		
		
		
		JButton btnStartButton = new JButton("Start");
		panel_4.add(btnStartButton);
		btnStartButton.setPreferredSize(btnSize);
		btnStartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				graphPanel.setVisible(true);
			}
		});
		
		JButton btnSaveButton = new JButton("Save");
		panel_4.add(btnSaveButton);
		btnSaveButton.setPreferredSize(btnSize);
		btnSaveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String settings = username + " " + theme;
				try {
					PrintWriter pw = new PrintWriter(new File("settings.txt"));
				    Files.write(Paths.get("settings.txt"), settings.getBytes(), StandardOpenOption.APPEND);
				} catch (IOException e) {
				    e.printStackTrace();
				}
			}
		});
		
		
		
		JButton btnExit = new JButton("Exit");
		panel_4.add(btnExit);
		btnExit.setPreferredSize(btnSize);
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ExitPopUp exit = new ExitPopUp();
			}
		});
		
		contentPane.setBackground(colors[0]);
		panel.setBackground(colors[0]);
		

		graphPanel = new StackPanel();
		contentPane.add(graphPanel, BorderLayout.CENTER);
		graphPanel.setVisible(false);
		
		java.net.URL iconURL = getClass().getResource("cat-paw.png");
		ImageIcon icon = new ImageIcon(iconURL);
		this.setIconImage(icon.getImage());
	}
	
	private int getUserTheme() {
		if (!userAuth || username == null) return 0;
		Scanner scan = null;
		try {
			scan = new Scanner(new File("settings.txt"));
		} catch (FileNotFoundException e) {
			return 0;
		} finally {
			scan.close();
		}
		while (scan.hasNextLine()) {
			String line = scan.nextLine();
	    	String[] words = line.split(" ");
	    	if (words[0].equals(username)) {
	    		if (words[1].equals("1")) return 1;
	    		if (words[1].equals("2")) return 2;
	    	}
		}
		scan.close();
    	return 0;
		
	}

}
