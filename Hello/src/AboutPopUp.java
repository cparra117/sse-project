import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

import java.awt.SystemColor;
import java.awt.Color;

public class AboutPopUp extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the window.
	 */
	public static void main(String[] args) {
		try {
			AboutPopUp dialog = new AboutPopUp();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the frame.
	 */
	public AboutPopUp() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(120, 120, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(Color.LIGHT_GRAY, 2));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextArea txtrDataStructure = new JTextArea();
		txtrDataStructure.setBackground(SystemColor.window);
		txtrDataStructure.setEditable(false);
		txtrDataStructure.setText("Data Structure Exploration Tool\nVersion 1.0");
		txtrDataStructure.setBounds(17, 19, 203, 32);
		contentPane.add(txtrDataStructure);
		
		JTextArea txtrAnastasiaTaing = new JTextArea();
		txtrAnastasiaTaing.setBackground(SystemColor.window);
		txtrAnastasiaTaing.setEditable(false);
		txtrAnastasiaTaing.setText("Anastasia Taing\nChristopher Parra\nThomas Howley\nHoan Nguyen\nZyrian Gantuangco");
		txtrAnastasiaTaing.setBounds(17, 95, 124, 80);
		contentPane.add(txtrAnastasiaTaing);
		
		java.net.URL iconURL = getClass().getResource("cat-paw.png");
		ImageIcon icon = new ImageIcon(iconURL);
		this.setIconImage(icon.getImage());
		
		this.setVisible(true);
	}
}
