import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;

public class SettingsPopUp extends JFrame {

	private JPanel contentPane;
	private JButton btnSubmit;
	private JPanel panel_2;
	private JPanel panel_1;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JRadioButton rdbtnDefaultgrey;
	private JRadioButton greenBtn;


	/**
	 * Create the frame.
	 */
	public SettingsPopUp(JPanel p1, JPanel p2, JPanel p3, Color[] colors) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(120, 120, 393, 246);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel_2 = new JPanel();
		panel_2.setBounds(0, 0, 382, 206);
		contentPane.add(panel_2);
		panel_2.setLayout(null);
		
		JRadioButton defaultBtn = new JRadioButton("Default");
		defaultBtn.setMnemonic(KeyEvent.VK_D);
	    defaultBtn.setActionCommand("Default");
	    defaultBtn.setSelected(true);
		defaultBtn.setBounds(129, 32, 103, 25);
		panel_2.add(defaultBtn);
		
		greenBtn = new JRadioButton("Pink");
		greenBtn.setMnemonic(KeyEvent.VK_P);
		greenBtn.setActionCommand("Pink");
	    greenBtn.setBounds(129, 62, 63, 31);
		panel_2.add(greenBtn);
		
		JRadioButton redBtn = new JRadioButton("Blue");
		redBtn.setMnemonic(KeyEvent.VK_B);
		redBtn.setActionCommand("Blue");
		redBtn.setBounds(129, 98, 127, 25);
		panel_2.add(redBtn);
		
		ButtonGroup group = new ButtonGroup();
		group.add(defaultBtn);
		group.add(greenBtn);
		group.add(redBtn);
		
		
		btnSubmit = new JButton("Submit Change");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int choice = 0;
				if (greenBtn.isSelected()) choice = 1;
				else if (redBtn.isSelected()) choice = 2;
				p1.setBackground(colors[choice]);
				p2.setBackground(colors[choice]);
				p3.setBackground(colors[choice]);
				setVisible(false);
				dispose();
			}
		});
		btnSubmit.setBounds(77, 134, 200, 31);
		panel_2.add(btnSubmit);
		
		
		
		//cat icon
		java.net.URL iconURL = getClass().getResource("cat-paw.png");
		ImageIcon icon = new ImageIcon(iconURL);
		this.setIconImage(icon.getImage());
		this.setVisible(true);
	}
}

