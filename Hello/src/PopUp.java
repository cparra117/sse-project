import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.CardLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;

import javax.swing.Box;


public class PopUp extends JFrame {

	private JPanel contentPane;
	private JButton btnLogin;
	private JPanel panel_2;
	private JPanel panel_1;
	private JLabel label;
	private JPasswordField passwordField;
	private JPanel panel;
	private JLabel label_1;
	private JTextField textField;
	private JButton btnNewAccount;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private boolean authenticated;
	private String username;
	private Component horizontalStrut;
	private Component horizontalStrut_1;
	private Component horizontalStrut_2;
	private Component horizontalStrut_3;
	private Component horizontalStrut_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PopUp frame = new PopUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the frame.
	 */
	public PopUp() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(120, 120, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel_2 = new JPanel();
		panel_2.setBounds(5, 5, 424, 251);
		contentPane.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel = new JPanel();
		panel_2.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		horizontalStrut_3 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_3);
		
		horizontalStrut_1 = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut_1);
		
		horizontalStrut = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut);
		
		label_1 = new JLabel("User");
		panel.add(label_1);
		
		textField = new JTextField();
		textField.setText("user");
		textField.setColumns(10);
		panel.add(textField);
		
		panel_1 = new JPanel();
		panel_2.add(panel_1);
		panel_1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		horizontalStrut_4 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_4);
		
		horizontalStrut_2 = Box.createHorizontalStrut(20);
		panel_1.add(horizontalStrut_2);
		
		label = new JLabel("Password");
		panel_1.add(label);
		
		passwordField = new JPasswordField();
		passwordField.setColumns(10);
		//passwordField.setText("password");
		panel_1.add(passwordField);
		
		label_2 = new JLabel("");
		panel_2.add(label_2);
		
		label_3 = new JLabel("");
		panel_2.add(label_3);
		
		label_4 = new JLabel("");
		panel_2.add(label_4);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				authenticated = AccountManager.authenticate(textField.getText(), passwordField.getText());
				if (!authenticated) {
					JOptionPane.showMessageDialog(null, "Invalid user-password pair");
				}
				else {
					username = textField.getText();
					setVisible(false); 
					dispose();
				}
			}
		});
		panel_2.add(btnLogin);
		
		
		btnNewAccount = new JButton("New Account");
		panel_2.add(btnNewAccount);
		
		
		
		//cat icon
		java.net.URL iconURL = getClass().getResource("cat-paw.png");
		ImageIcon icon = new ImageIcon(iconURL);
		this.setIconImage(icon.getImage());
		
		this.setVisible(true);

	}
	
	public boolean authenticated() {
		return authenticated;
	}
	
	public String getUser() {
		return username;
	}

}
