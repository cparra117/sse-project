import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExitPopUp extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the window.
	 */
	public static void main(String[] args) {
		try {
			ExitPopUp dialog = new ExitPopUp();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ExitPopUp() {
		getContentPane().setBackground(UIManager.getColor("Button.background"));
		setBounds(120, 120, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(UIManager.getColor("Button.background"));
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.WEST);
		{
			JTextArea txtrAreYouSure = new JTextArea();
			txtrAreYouSure.setFont(new Font("Segoe UI", Font.PLAIN, 12));
			txtrAreYouSure.setBackground(UIManager.getColor("Button.background"));
			txtrAreYouSure.setText("Are you sure you want to exit?\r\n\r\nMake sure to save progress before exiting.\r\nAny unsaved progress will be lost.");
			txtrAreYouSure.setEditable(false);
			contentPanel.add(txtrAreYouSure);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Yes, exit program");
				okButton.setFont(new Font("Tahoma", Font.BOLD, 13));
				okButton.setActionCommand("OK");
				okButton.setPreferredSize(new Dimension(150, 30));
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						System.exit(0);
					}});
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.setPreferredSize(new Dimension(80, 30));
				cancelButton.setFont(new Font("Tahoma", Font.BOLD, 13));
				buttonPane.add(cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}});
			}
		}
		
		java.net.URL iconURL = getClass().getResource("cat-paw.png");
		ImageIcon icon = new ImageIcon(iconURL);
		this.setIconImage(icon.getImage());
		this.setVisible(true);
	}

}
