import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.UIManager;

public class HelpPopUp extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					HelpPopUp frame = new HelpPopUp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public HelpPopUp() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(120, 120, 470, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		Dimension btnSize = new Dimension(120, 30);
		
		Panel panel = new Panel();
		panel.setPreferredSize(new Dimension(120, 260));
		contentPane.add(panel, BorderLayout.WEST);
		Panel panel_1 = new Panel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JTextArea txtrAStackIs = new JTextArea();
		txtrAStackIs.setBackground(UIManager.getColor("Button.background"));
		txtrAStackIs.setEditable(false);
		txtrAStackIs.setWrapStyleWord(true);
		txtrAStackIs.setFont(new Font("Dialog", Font.PLAIN, 11));
		txtrAStackIs.setText("A stack is a last-in, first out data structure. \r\nThis means that the last item put in the stack\r\nwill be the first one that a user will be able to remove. \r\nItems are added and removed one at a time from stacks. \r\nItems can be added to a stack using the push() method.\r\nItems can be removed using the pop() method. \r\nDrag the balls into the empty silhouttes and source code\r\nwill appear representing the  corresponding stack operation.");
		panel_1.add(txtrAStackIs);
		txtrAStackIs.setVisible(false);
		
		JButton btnStacks = new JButton("Stacks");
		btnStacks.setPreferredSize(btnSize);
		panel.add(btnStacks);
		btnStacks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(txtrAStackIs.isVisible() == false)
				{	
				txtrAStackIs.setVisible(true);
				}
				else
				{
					txtrAStackIs.setVisible(false);
				}
			}});
		
		JButton btnNewButton = new JButton("Queues");
		btnNewButton.setPreferredSize(btnSize);
		panel.add(btnNewButton);
		
		JButton btnLinkedLists = new JButton("Linked Lists");
		btnLinkedLists.setPreferredSize(btnSize);
		panel.add(btnLinkedLists);
		
		java.net.URL iconURL = getClass().getResource("cat-paw.png");
		ImageIcon icon = new ImageIcon(iconURL);
		this.setIconImage(icon.getImage());
		
		this.setVisible(true);
		
		
	}

}
